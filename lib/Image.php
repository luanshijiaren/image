<?php
    //创建验证码图片

    /**
     * @param int $type   验证码类型
     * @param int $length 验证码长度
     * @param int $pixel 点的干扰线多少点
     * @param int $line  直线干扰线要多少条
     * @param string $sess_name
     */
    function verifyImage($type=1,$length=4,$pixel=0,$line=0,$sess_name='verify'){
       


        $width=80;
        $height=28;
        //创建画布
        //创建GD库做验证码
        $image=imagecreatetruecolor($width,$height);
        $white=imagecolorallocate($image,255,255,255);
        $black=imagecolorallocate($image,0,0,0);
        //用填充矩形填充画布
        imagefilledrectangle($image,1,1,$width-2,$height-2,$white);
        if($type==1){
            $chars=join('',range(0,9));
        }elseif($type==2){
            //join是把数组拼装一起
            $chars=join('',array_merge(range('a','z'),range('A','Z')));
        }elseif($type==3){
            $chars=join('',array_merge(range('a','z'),range('A','Z'),range(0,9)));
        }
        //判断一下验证码长度是否符合
        if($length>strlen($chars)){
            //输出一条信息 结束当前脚本
            exit('字符串长度不够');
        }
        //随机打乱一下字符
        $chars=str_shuffle($chars);
        //取字符长度0-$length
        $chars= substr($chars,0,$length);
        //超全局变量储存
        session_start();
        $_SESSION[$sess_name]=$chars;
        $fontfiles=array('msyh.ttc','msyhbd.ttc','msyhl.ttc','simsun.ttc');
        for($i=0;$i<$length;$i++){
            //字体随机14-18
            $size=mt_rand(14,18);
            $angle=mt_rand(-15,15);
            $x=5+$i*$size;
            $y=mt_rand(20,26);
            $color=imagecolorallocate ($image, mt_rand(50,90), mt_rand(80,200), mt_rand(90,180));

            $fontfile ='./fonts/'.$fontfiles[mt_rand(0,count($fontfiles)-1)];

//            $fontfile='../fonts/'.$fontfiles[mt_rand(0,count($fontfiles)-1)];
            $text =substr($chars,$i,1);
            imagettftext($image,$size,$angle,$x,$y,$color,$fontfile,$text);
        }
        //画布的干扰点
        if($pixel){
            for($i=1;$i<$pixel;$i++){
                imagesetpixel($image,mt_rand(0,$width-1),mt_rand(0,$height-1),$black);

            }
        }
        //画布的干扰线
        if($line){
            for($i=1;$i<$line;$i++){
                $color=imagecolorallocate ($image, mt_rand(50,90), mt_rand(80,200), mt_rand(90,180));
                imageline($image,mt_rand(0,$width-1),mt_rand(0,$height-1),mt_rand(0,$width-1),mt_rand(0,$height-1),$color);
            }
        }
		header('content-type:image/gif');//告诉浏览器输入什么内容
        imagegif($image);//创建画布
        imagedestroy($image);//删除画布
    }
 









